/**
 * @dev the bopLevels are the level definition for the bops
 */
const networkConfig = {
  1337: {
    name: 'localhost',
  },
  31337: {
    name: 'hardhat',
  },
  80001: {
    name: 'mumbai',
  },
  137: {
    name: 'polygon',
  },
};

function one(value = 1, left = 0, right = 18) {
  let from = ethers.BigNumber.from('1' + '0'.repeat(left));
  let to = ethers.BigNumber.from('1' + '0'.repeat(right));
  return ethers.BigNumber.from(value).mul(to).div(from);
}

console.log(one(5, 2))

module.exports = {
  networkConfig,
};
