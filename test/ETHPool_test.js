const { ethers, waffle } = require('hardhat');

const { assert, expect } = require('chai');

describe('ETHPool', async function () {
  const parseAmount = (amount) => ethers.utils.parseEther(amount);
  const parseReverse = (amount) => ethers.utils.formatEther(amount);
  const provider = waffle.provider;

  beforeEach(async () => {
    // Using fixture from hardhat-deploy
    await deployments.fixture(['ethpool']);
    
    /* address */
    [owner, address1, address2] = await ethers.getSigners();

    /* Contract */
    ethpool = await ethers.getContract('ETHPool');    


  });

  describe('deposit', async () => {
    it('Deposit 200 and check that they are recorded', async () => {

      await ethpool.deposit({value: parseAmount("200")})

      const balance = await ethpool.totalDeposits();

      expect(balance.toString()).to.equal(parseAmount("200"));

    });

    it('Deposit 200, withdraw and check that balance is zero', async () => {

      await ethpool.deposit({value: parseAmount("200")})

      await ethpool.claimRewards();

      const balance = await provider.getBalance(ethpool.address);

      expect(balance.toString()).to.equal(parseAmount("0"));

    });

    it('Deposit and then withdraw. Check deposit value', async () => {
      
      const deposits1 = await ethpool.totalDeposits();
      expect(deposits1).to.equal(parseAmount("0"));


      await ethpool.deposit({value: parseAmount("1")})

      const deposits2 = await ethpool.totalDeposits();
      expect(deposits2).to.equal(parseAmount("1"));

      await ethpool.claimRewards();

      const deposits3 = await ethpool.totalDeposits();
      expect(deposits3).to.equal(parseAmount("0"));

    });

    it('Deposit 0. Should revert', async () => {
      await expect(ethpool.deposit({value: parseAmount("0")})).to.be.reverted;
    });

    it('Deposit twice. Should revert', async () => {
      await ethpool.deposit({value: parseAmount("100")});
      await expect(ethpool.deposit({value: parseAmount("100")})).to.be.reverted;
    });

    it('Deposit withdraw and deposit again', async () => {
      await ethpool.deposit({value: parseAmount("100")});
      await ethpool.claimRewards();
      await ethpool.deposit({value: parseAmount("100")});
    });
  });

  describe('depositRewards', async () => {
    it('Deposit 200 rewards. Check balances', async () => {
      await ethpool.depositRewards({value: parseAmount("1")});

      const rewardsBalance = await ethpool.currentRewards();
      expect(rewardsBalance).to.equal(parseAmount("1"));

      const contractBalance = await provider.getBalance(ethpool.address);
      expect(contractBalance).to.equal(parseAmount("1"));

      const depositsBalance = await ethpool.totalDeposits();
      expect(depositsBalance).to.equal(parseAmount("0"));
    });

    it('Deposit 200 and 200 rewards. Check that they do not mix', async () => {
      await ethpool.depositRewards({value: parseAmount("1")});
      await ethpool.connect(address1).deposit(
        {value: parseAmount("1")}
      );
      const rewardsBalance = await ethpool.currentRewards();
      expect(rewardsBalance).to.equal(parseAmount("1"));

      const depositsBalance = await ethpool.deposits(address1.address);
      expect(rewardsBalance).to.equal(parseAmount("1"));


      const contractBalance = await provider.getBalance(ethpool.address);
      expect(contractBalance).to.equal(parseAmount("2"));

    });

    it('Deposit 0 rewards. Should revert', async () => {
      await expect(ethpool.depositRewards({value: parseAmount("0")})).to.be.reverted;
    });

  });

  describe('claimRewards', async () => {
    it('A deposits 100, and B deposits 300 for a total of 400 in the pool. \n \
      Now A has 25% of the pool and B has 75%. \n \
      When T deposits 200 rewards, A should be able to withdraw 150 and B 450.', async () => {

      await ethpool.connect(address1).deposit({value: parseAmount("100")});
      await ethpool.connect(address2).deposit({value: parseAmount("300")});

      const balanceA = await provider.getBalance(address1.address);
      const balanceB = await provider.getBalance(address2.address);

      await ethpool.depositRewards({value: parseAmount("200")});
      await ethpool.connect(address1).claimRewards();
      await ethpool.connect(address2).claimRewards();

      const newBalanceA = await provider.getBalance(address1.address);
      const newBalanceB = await provider.getBalance(address2.address);

      expect(newBalanceA.sub(balanceA)).to.equal(parseAmount("150"));
      expect(newBalanceB.sub(balanceB)).to.equal(parseAmount("450"));
      

    });

    it('A deposits then T deposits then B deposits  \n \
    then A withdraws and finally B withdraws.       \n \
    A should get their deposit + all the rewards.   \n \
    B should only get their deposit because rewards \n \
    were sent to the pool before they participated.', async () => {

      await ethpool.connect(address1).deposit({value: parseAmount("1000")});
      await ethpool.depositRewards({value: parseAmount("500")});
      await ethpool.connect(address2).deposit({value: parseAmount("500")});

      const balanceA = await provider.getBalance(address1.address);
      const balanceB = await provider.getBalance(address2.address);

      await ethpool.connect(address1).claimRewards();
      await ethpool.connect(address2).claimRewards();

      const newBalanceA = await provider.getBalance(address1.address);
      const newBalanceB = await provider.getBalance(address2.address);

      expect(newBalanceA.sub(balanceA)).to.equal(parseAmount("1500"));
      expect(newBalanceB.sub(balanceB)).to.equal(parseAmount("500"));
      

    });

    it('Claim without deposit. Should revert', async () => {
      await expect(ethpool.connect(address1).claimRewards()).to.be.revertedWith('No deposits for address');
    });

  });

});
