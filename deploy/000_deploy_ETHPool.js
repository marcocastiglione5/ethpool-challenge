module.exports = async ({ getNamedAccounts, deployments }) => {
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // this contract is upgradeable through uups (EIP-1822)
  const RoyaltyFactory = await deploy('ETHPool', {
    from: deployer,
    log: true,
    proxy: {
      proxyContract: 'UUPSProxy',
      execute: {
        init: {
          methodName: 'initialize', // method to be executed when the proxy is deployed
          args: [],
        },
      },
    },

    args: []
    
  });

};

module.exports.tags = ['ethpool'];
