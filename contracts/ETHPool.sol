// SPDX-License-Identifier: MIT

pragma solidity 0.8.4;

import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract ETHPool is Initializable, AccessControl, UUPSUpgradeable {

    bytes32 public constant TEAM = keccak256("TEAM");

    struct Cycle {
        uint256 accruedRewards;
        uint256 totalDeposits;
    }

    /// @dev cycle ID => Cycle data
    mapping (uint256 => Cycle) private _rewardCycles;

    /// @dev current rewards cycle
    uint256 public currentCycle;

    /// @dev when using claimRewards function, look for rewards for how many cycles
    uint256 private _claimRewardsHowManyCycles;

    /// @notice total rewards amount;
    uint256 public currentRewards;

    /// @dev address => deposits
    mapping(address => uint256) public deposits;

    /// @notice total deposits in ETH
    uint256 public totalDeposits;

    /// @notice last cycle claimed by address
    mapping (address => uint256) public addressLastClaimed;

    /// @dev address => last deposit cycle
    mapping (address => uint256) private _lastDeposit;

    event Deposit(
        address owner,
        uint256 value
    );

    event DepositRewards(
        address sender,
        uint256 value
    );

    constructor() {}

    function initialize() external initializer {
        _claimRewardsHowManyCycles = 26;

        _nextCycle();

        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(TEAM, msg.sender);
    }

    /// @notice deposit ETH
    function deposit() external payable {
        require(msg.value > 0, "Value must be greater than 0");
        require(deposits[msg.sender] == 0, "Must withdraw first!");

        deposits[msg.sender] = msg.value;
        _lastDeposit[msg.sender] = currentCycle;
        _rewardCycles[currentCycle].totalDeposits += msg.value;

        totalDeposits += msg.value;
        emit Deposit(msg.sender, msg.value);
    }

    /// @notice deposit rewards. Can only be called by team members
    function depositRewards() external payable onlyRole(TEAM) {
        require(msg.value > 0, "Value must be greater than 0");
        _rewardCycles[currentCycle].accruedRewards += msg.value;
        currentRewards += msg.value;
        // each time rewards are deposited, change rewards cycle
        _nextCycle();
        emit DepositRewards(msg.sender, msg.value);
    }

    /// @dev change cycle and init cycle data
    function _nextCycle() internal {
        currentCycle += 1;
        _rewardCycles[currentCycle] = Cycle(0,0);
    }

    /// @notice claim rewards with default range
    function claimRewards() external {

        uint256 lastDeposit = _lastDeposit[msg.sender];
        require(lastDeposit > 0, "No deposits for address");

        uint256 depositAmount = deposits[msg.sender];
        
        // add reward
        uint256 reward = deposits[msg.sender];

        if (currentCycle > lastDeposit) {
            uint256 accruedRewards = _rewardCycles[lastDeposit].accruedRewards;
            uint256 rewardAux = accruedRewards * depositAmount / _rewardCycles[lastDeposit].totalDeposits;

            reward += rewardAux;
            _rewardCycles[lastDeposit].accruedRewards -= rewardAux;
            addressLastClaimed[msg.sender] = lastDeposit;
        } 

        // Update values
        deposits[msg.sender] = 0;
        _rewardCycles[lastDeposit].totalDeposits -= depositAmount;        
        totalDeposits -= depositAmount;

        payable(msg.sender).transfer(reward);
    }

    function setClaimRewardsHowManyCycles(uint256 claimRewardsHowManyCycles_) external onlyRole(TEAM) {
        _claimRewardsHowManyCycles = claimRewardsHowManyCycles_;
    }

    function _authorizeUpgrade(address newImplementation) internal override onlyRole(DEFAULT_ADMIN_ROLE) {}

}